/**
 * Le DOM (Document Object Model) est la représentation sous formes d'objets
 * du HTML interprété de la page actuelle.
 * Le Javascript peut interagir avec ce DOM et le modifier comme il souhaite.
 * On peut changer tous les attributs html des éléments, rajouter des éléments,
 * supprimer des éléments, etc.
 * Tous ces changements s'appliquent au DOM de la page actuelle, le HTML en lui même 
 * reste inchangé (heureusement)
 */

//Pour sélectionner un élément, on utilise la méthode querySelector du document qui attend
//en argument un sélecteur CSS. Cette méthode renvoie un object Element/HTMLElement
let para = document.querySelector('#para');

//Sur l'élément en question, on peut modifier les propriétés qu'on souhaite
para.style.color = '#b10b10';
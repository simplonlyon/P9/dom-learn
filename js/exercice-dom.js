/*
  JS DOM Exercise

  1. Mettre le texte de l'élément avec l'id para2 en bleu
  2. Mettre une border en pointillet noire à la section2
  3. Mettre une background color orange à l'élément de la classe colorful de la section2 
  4. Mettre le h2 de la section1 en italique
  5. Cacher l'élément colorful situé dans un paragraphe
  6. Changer le texte de para2 pour "modified by JS"
  7. Changer le href du lien de la section1 pour le faire aller sur simplonlyon.fr
  8. Rajouter la classe bigText sur le h2 de la section2

  Bonus : Faire que tous les paragraphe du document soit en italique

 */

 let para2 = document.querySelector('#para2');
 para2.style.color = 'blue';

 let section2 = document.querySelector('#section2');
 section2.style.border = 'dashed';

 let colorful = document.querySelector ('#section2 .colorful');
 colorful.style.backgroundColor = 'orange';

let italic = document.querySelector('#section1 h2');
italic.style.fontStyle = 'italic';

let hide = document.querySelector('p span');
hide.style.display = 'none';

// para2.innerHTML = 'Modified by Js';
para2.textContent = 'Modified by Js';

let changeHref = document.querySelector('a');
changeHref.href = 'https://www.simplonlyon.fr/';

// colorful.className += ' bigText';
colorful.classList.add('bigText');

/**
 * On utilise le querySelectorAll pour sélectionner tous les éléments
 * correspondant au sélecteur css. Celui ci nous renvoie un tableau
 * d'éléments html (un pseudo tableau en l'occurrence mais peu importe)
 */
let paras = document.querySelectorAll('p');

/**
 * Si je veux appliquer une modification sur tous les éléments du tableau
 * je dois parcourir mon tableau ("itérer sur le tableau") et appliquer
 * la modification sur chaque élément du tableau. Je le fais ici avec
 * un for...of (mais un for, ou while ou un paras.forEach auraient été
 * possibles également)
 */
for(let item of paras) {
    item.style.fontStyle = 'italic';
}



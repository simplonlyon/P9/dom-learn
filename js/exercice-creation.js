/*
Dom create Exercices

1. Créer un élément de type paragraphe et le mettre dans une variable
2. Assigner à cet élément comme textContent "created by js"
3. mettre cet élément dans la section #target qui est dans le HTML
4. Ajouter un event au double click sur l'élément paragraphe qui affichera "coucou" en console
5. Rajouter un bouton dans le fichier HTML et lui mettre un event au click qui créera un nouvel élément paragraphe pour le mettre dans la section target
6. Créer une variable count initialisée à 0, et faire qu'à chaque fois qu'on click sur le bouton, elle s'incrémente de 1
7. Assigner au textContent de l'élément créé par le bouton la valeur de la variable count
8. Rajouter un event au double click sur les éléments créés par le bouton qui supprimera l'élément en question (.remove()) et qui décrémentera la variable count de 1

*/
let target = document.querySelector('#target');
let btn = document.querySelector('.btn');
let para = document.createElement('p');
let count = 0;

para.textContent = "created by JS";
target.appendChild(para);

para.addEventListener('dblclick', function () {
    console.log('coucou');

});

btn.addEventListener('click', function () {
    let p = document.createElement('p');
    target.appendChild(p);
    p.textContent = "Je compte " + count;
    count++;
    
    p.addEventListener('dblclick', function () {
        p.remove();
        count--;

    });

});



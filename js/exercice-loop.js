/**
Exercices tableau + boucles
1. Créer un array (tableau) de valeurs textuelles et le mettre dans une variable tab
2. Afficher dans la console la 3ème valeur du tableau
3. Ajouter "modified" à la deuxième valeur du tableau (concaténation)
4. En vous inspirant de la boucle for..of faite dans le fichier exercice-dom.js, parcourir le tableau et afficher en console chacune de ses valeurs
4bis. (optionnel) Faire la même chose avec un for classique (et avec un while ?)
5. Créer une variable dans laquelle vous concaténerez chaque valeur du tableau avec une boucle avant d'afficher la variable
6. Créer une variable count initialisée à 0 puis faire une boucle sur le tableau qui incrémentera cette variable SI la valeur actuelle de la boucle fait plus de 3 caractères (le .length permet de compter le nombre de caractères d'une string)

 */

let tab = ['valeur1', 'valeur2', 'val', 'va', 'v'];

console.log(tab[2]);

tab[1] += ' modified';

for (const item of tab) {
    console.log(item);
}

// for (let index = 0; index < tab.length; index++) {
//     console.log(tab[index]);
// }

// let compteur = 0;
// while(compteur < tab.length) {
//     console.log(tab[compteur]);
//     compteur++;
// }

let sentence = '';
for (const item of tab) {
    sentence += item+' ';
}
console.log(sentence);

// console.log(tab.join(' '));

let count = 0;
for (const item of tab) {
    if(item.length > 3) {
        count++;
    }
}
console.log(count);

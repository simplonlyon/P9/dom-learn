/*
Exercice Function + DOM
1. En JS, comme on a déjà fait, changer la couleur du texte du para1 en blue, celle du para2 en red et celle du para3 en orange
2. En regardant le code obtenu, vous devriez remarquer qu'il se répète pas mal, identifiez les morceaux qui change
3. Faire une fonction changeColor qui attendra plusieurs argument (ceux que vous avez identifié) qui permettra de changer la couleur de n'importe quel élément en lui donnant n'importe quelle couleur
4. Hors de la fonction, faire une selection et une boucle pour modifier la couleur de tous les élément avec la classe color en vert
5. En utilisant le querySelectorAll et un if, essayer d'ajouter à la fonction changeColor le fait que si plusieurs éléments correspondent au sélecteur, ça fera la boucle pour changer la couleur de chacun d'entre eux
*/

// let para1 = document.querySelector('#para1');
// para1.style.color = 'blue';
// let para2 = document.querySelector('#para2');
// para2.style.color = 'orange';
// let para3 = document.querySelector('#para3');
// para3.style.color = 'green';

changeColor('#para1', 'blue');
changeColor('#para2', 'orange');
changeColor('#para3', 'red');



function changeColor(selector, color) {
    let element = document.querySelector(selector);
    element.style.color = color;
}

/**
 * 
 * V2 pour exercice 5
 */
//changeColor('.color', 'green');
// function changeColor(selector, color) {

//     let elementList = document.querySelectorAll(selector);
//     for (const element of elementList) {
//         element.style.color = color;
//     }
// }
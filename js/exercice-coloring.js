/*
Exercice Event + Function
1. Dans un nouveau fichier exercice-coloring.html / exercice-coloring.js, créer un input type color et un paragraphe avec une class "colorable"(modifié)
2. Dans le javascript, rajouter un event au click sur le paragraphe qui changera sa color en red
3. Dans ce même event, faire en sorte de récupérer la value de l'input et en faire un console.log (comme on a déjà fait avant)
4. Une fois qu'on a récupéré la value de l'input, l'utiliser pour faire que la couleur du paragraphe devienne celle indiquée dans l'input
5. Rajouter 3 autres paragraphes, tous avec la classe "colorable" et faire un querySelectorAll pour ajouter l'eventListener fait précédemment sur tous les éléments ayant cette classe (il faudra utiliser une boucle, genre un for...of)
*/
//On crée une variable colors dans laquelle on met tous les éléments
//du DOM correspondant à la classe colorable
let colors = document.querySelectorAll(".colorable");
//on initialise une variable dans laquelle se trouve l'élément avec id inp
let inputColor = document.querySelector("#inp");
//initialisation d'une boucle for..of qui parcours tous les éléments du tableau colors
for (const item of colors) {
    console.log('boucle1');
//ajouter un évènement sur chaque item de colors qui fera qu'au click ya une fonction qui se déclenche
    item.addEventListener("click", function () {
        //initialisation d'une boucle for..of qui parcours tous les éléments du tableau colors
        for (const iterator of colors) {
            console.log('bloucle2');
            //on déclare que la couleur des iterator de la variable colors prend la value de l'input
            iterator.style.color = inputColor.value;
            //on affiche dans la console la value de la variable inputColor
            // console.log(inputColor.value);
            
        }
    });
    
}
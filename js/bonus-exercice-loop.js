/*

Exercices Boucles for..of :
1. Créer un tableau avec `['un', 'deux', 'trois', 'quatre']` dedans
2. faire une boucle for..of pour afficher en console chaque valeur de la boucle
3. Faire une boucle for..of qui affichera le nombre de caractère de chaque élément de la boucle (avec .length)
4. Faire une variable et une boucle for..of qui stockera dans la variable le total de caractères de tous les mots de la boucle
5. Faire une boucle for..of qui affiche les valeurs du tableau sauf si cette valeur est 'un' ou 'trois'
*/
/*
1. Créer une fonction qui crée un élément paragraphe et l'append dans l'élément avec l'id target
2. Modifier la fonction pour faire en sorte qu'avec 2 arguments on puisse choisir quel type d'élément créer et où l'ajouter
3. Faire en sorte que la fonction renvoie l'élément créé afin de pouvoir continuer à interagir avec en dehors de celle ci
4. En utilisant cette fonction, créer 3 éléments : 
* une div qui ira dans l'élément #target et qui aura "je suis une div" en textContent
* un p qui ira dans l'élément #target et qui aura "je suis un p" en textContent
* un p qui ira dans l'élément div créé précédemment qui aura "je suis un p dans une div" en textContent

*/

let divElement = create('div', '#target');
divElement.textContent = 'je suis une div';

let pElement = create('p', '#target');
pElement.textContent = 'je suis un p';

let pInDiv = create('p', '#target>div');
pInDiv.textContent = 'je suis un p dans une div';



function create(type, selector) {
    let element = document.createElement(type);
    let target = document.querySelector(selector);

    target.appendChild(element);

    return element;
}

/*
//version chépèr
let section = document.querySelector("#target");

let divElement = create('div', section);
divElement.textContent = 'je suis une div';

let pElement = create('p', section);
pElement.textContent = 'je suis un p';

let pInDiv = create('p', divElement);
pInDiv.textContent = 'je suis un p dans une div';



function create(type, target) {
    let element = document.createElement(type);

    target.appendChild(element);

    return element;
}
*/
let btn = document.querySelector('#btn');
/**
 * On peut rajouter des Event (événements) sur les éléments capturés
 * en Javascript. En gros on leur dit "voilà ce qu'il faudra déclencher
 * lorsque l'événement en question sera détecté sur cet élément"
 * Ici, on dit au javascript de faire un console.log lorsque l'on click
 * sur l'élément avec l'id #btn 
 * Side note : On utilise pour ce faire une fonction anonyme, une fonction
 * qui n'a pas de nom en somme et qui n'est pas destinée à être réutilisée
 * ailleurs dans le code, on la crée pour cet endroit spécifique.
 * On pourrait techniquement utiliser une fonction nommée à la place, 
 * mais c'est pas obligatoire
 */
btn.addEventListener('click', function() {
    console.log('bloup');
});

//manière alternative de déclarer un event
// btn.onclick = function() {

// };
//On alors avec directement un onclick en attribut dans le html


document.querySelector('body').addEventListener('click', function(event) {
    console.log(event);
});